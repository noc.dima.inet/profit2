<?php

use  App\{Pages, Article, View};

require __DIR__ . '/autoload.php';

$view = new View;

$view->title = Pages::findByName('admin')->title;

$view->allNews = Article::findAll();
$view->display( __DIR__ . '/admin/template/index.php');