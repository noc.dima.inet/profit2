<?php


class Message
{

    public function __construct()
    {
    }
    

    public function messageLogs($errorSessionName) 
    {
        $arrSessionName=[
            0=>[
                "name"=>"errorPwd",
                "messsage"=>"пароль не верный"
            ],           
            1=>[
                "name"=>"noExistsUser",
                "messsage"=>"юзера с таким именем нет"
            ],
            2=>[
                "name"=>"emptyPwd",
                "messsage"=>"Поле пароля не м.б. пустым"
            ],
            3=>[
                "name"=>"existsUser",
                "messsage"=>"Юзер c таким именем уже существует"
            ],
            4=>[
                "name"=>"emptyName",
                "messsage"=>"Поле имя юзера не м.б. пустым"
            ],             
            5=>[
                "name"=>"login",
                "messsage"=>"Авторизация"
            ],  
            6=>[
                "name"=>"logout",
                "messsage"=>"Вышел"
            ], 
            7=>[
                "name"=>"imgUploadOk",
                "messsage"=>"Загрузил картинку"
            ],               
            8=>[
                "name"=>"imgUploadError",
                "messsage"=>"Ошибка загрузки картинки"
            ],
            9=>[
                "name"=>"emptyEmail",
                "messsage"=>"Поле E-mail не м.б. пустым"
            ], 
            10=>[
                "name"=>"existsEmail",
                "messsage"=>"Email уже существует в базе"
            ],      
            11=>[
                "name"=>"voiceError",
                "messsage"=>"Что то поло не так, попробуйте проголосовать снова"
            ], 
            12=>[
                "name"=>"voiceOk",
                "messsage"=>"Спасибо, Вы проголосовали!"
            ], 
            13=>[
                "name"=>"emptyImg",
                "messsage"=>"Вы не прикрепили картинку"
            ],     
            14=>[
                "name"=>"emptyFields",
                "messsage"=>"Что то с полями не то"
            ],  
            15=>[
                "name"=>"uploadError",
                "messsage"=>"Ошибка загрузки в базу"
            ],                 
            16=>[
                "name"=>"uploadOk",
                "messsage"=>"Успешно загружено в БД"
            ],                                                                                      
            17=>[
                "name"=>"delField",
                "messsage"=>"Запись из БД удалена"
            ],                                                                                      
        ];
        for($i=0;$i<count($arrSessionName);$i++)
        {
            if($arrSessionName[$i]['name'] === $errorSessionName)
                return $arrSessionName[$i]['messsage'];
        }
 
    }
}