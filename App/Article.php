<?php

namespace App;

use App\Model;

/**
 *  @property \App\Article
 */
class Article extends Model
{

    /**  
     * @property int|null $author_id 
     * @property string $text  
     * @property string $description 
     */

    public int|null $author_id;
    public string $description;
    public string $text;

    protected static string $table = 'news';

    public function __get($name)
    {
        if ($name === 'author') {
            return Authors::findById($this->author_id)->author ?? 'Аноним';
        }
    }
}
