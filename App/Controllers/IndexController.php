<?php

namespace App\Controllers;

use App\View;
use App\Pages;
use App\Article;
// use App\Authors;
// use App\News;


require_once __DIR__ . '/../../App/Db.php';
require_once __DIR__ . '/../../App/Model.php';
require_once __DIR__ . '/../../App/Pages.php';
require_once __DIR__ . '/../../App/Article.php';
require_once __DIR__ . '/../../App/Authors.php';
require_once __DIR__ . '/../../App/News.php';

class IndexController
{

    public function action()
    {
        $view = new View;

        $view->title = Pages::findByName('index')->title;
        
        $view->limitNews = Article::findLimit(3);
        
        $view->display(__DIR__ . '/../../template/index.php');
    }

}