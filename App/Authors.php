<?php

namespace App;

use App\Model;

class Authors extends Model
{
    public string|null $author;

    protected static string $table = 'authors';

}