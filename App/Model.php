<?php

namespace App;

abstract class Model
{

    public int $id;

    protected static string $table = '';

    public static function findAll(): array
    {
        $db = new Db;
        $data = $db->query(
            'SELECT * FROM ' . static::$table,
             [],
             static::class
        );
        return $data;
    }


    public static function findById($id): object|null
    {
        $db = new Db;
        $data = $db->query(
            'SELECT * FROM ' . static::$table . " WHERE id = :id",
             [':id' =>  $id],
             static::class
        );

        return !empty($data) ? $data[0] : null;
    }



    public static function findByName($name): object|null
    {
        $db = new Db;
        $data = $db->query(
            'SELECT * FROM ' . static::$table . " WHERE name = :name",
             [':name' =>  $name],
             static::class
        );
        // var_dump($data);die;

        return !empty($data) ? $data[0] : null;
    }
    
    public static function findNameAuthor($author): object|null
    {
        $db = new Db;
        $data = $db->query(
            'SELECT * FROM ' . static::$table . " WHERE author = :author",
             [':author' =>  $author],
             static::class
        );

        return !empty($data) ? $data[0] : null;
    }

    public static function findLimit($limit): array
    {
        $db = new Db;
        $data = $db->query(
            'SELECT * FROM ' . static::$table . " ORDER BY id DESC LIMIT " .$limit,
             [],
             static::class
        );
        return $data;
    }

    public static function joinTableAndLimit($limit): array
    {
        $db = new Db;
        $data = $db->query(
            'SELECT * FROM ' . static::$table .
            ", authors WHERE authors.id=news.author_id ORDER BY news.id DESC LIMIT " .$limit,
             [],
             static::class
        );
        return $data;
    }

    public function update()
    {
        $sets = [];
        $data = [];
        foreach(get_object_vars($this) as $prop => $value)
        {
            $data[':'. $prop] = $value;
            if('id' == $prop) {
                continue;
            }
            $sets[] = $prop.'=:'.$prop;
        }
        $sql = "UPDATE " . static::$table . " SET " . implode(',', $sets) . " WHERE id=:id";
        // var_dump($sql); die;
        $db = new Db();
        $db->execute($sql,$data);
    }

    public function insert() {

        $sets = [];
        $val = [];
        $data = [];
        foreach(get_object_vars($this) as $prop => $value)
        {
            if('id' == $prop) {
                continue;
            }
            $data[':'. $prop] = $value;

            $sets[] = "`".$prop."`";
            $val[] = ":".$prop;
        }
          $sql = "INSERT INTO " . static::$table . " ( " . implode(',', $sets) . ") VALUES (" . implode(',', $val) . ")";

          $db = new Db();
          $res = $db->execute($sql,$data);
          return $db->lastInsertId();
        } 

    public function delete() { 
        $sql = "DELETE FROM " . static::$table . " WHERE id=:id";
        $db = new Db();
        $db->execute($sql,[':id' =>  $this->id]);
        
    }  

    public function save()
    {
       if(null !== $this->findById($this->id) )
       {
        return $this->update();
       } else {
        return $this->insert();
       }


    }



} // fin abstract class Model
