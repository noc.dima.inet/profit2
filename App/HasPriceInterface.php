<?php


interface HasPriceInterface
{
    
    public function getPrice() :float;

}