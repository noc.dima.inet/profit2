<?php

namespace App;

use App\Model;

class News extends Model
{
    public string $description;
    public string $text;
    public string $author;
    public int|null $author_id;

    protected static string $table = 'news';



}