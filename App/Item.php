<?php

namespace App;

use App\Model;

class Item extends Model implements HasPriceInterface,HasWeightInterface
{
    use HasPriceTrait;
    
    public string $name;

    protected static string $table = 'items';

    public function getWeight(): float
    {
        return $this->weight;
    }  

}