<?php

namespace App;

trait GetAndSetObjectTrait
{
    protected array $data;

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get(string $name)
    {
        return $this->data[$name];
    }

}
