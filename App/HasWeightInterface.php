<?php


interface HasWeightInterface
{
    public function getWeight() :float;

}