<?php

trait HasPriceTrait
{
    
    protected float $price;

    public function getPrice(): float
    {
        return $this->price;
    }

}