<?php

namespace App;

class View
{
    use GetAndSetObjectTrait;

    public function __construct()
    {
         
    }

    public function display(string $templateView)
    {
        echo $this->render($templateView);
    }

    public function render(string $templateView): string
    {
       ob_start();
        foreach($this->data as $key => $value) {
            $$key = $value; 
        }
        include_once $templateView;
        $out = ob_get_contents();
       ob_end_clean();
       return $out;
    }


}