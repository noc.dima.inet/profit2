<?php

namespace App;

use App\Config;
use PDO;

class Db {
    
    protected $data;
    protected PDO $dbh;

    public string $dsn;
    public string $user;
    public string $password;

    public function __construct()
    {
        $this->connect();
    } 

    private function connect()
    {
       $config = new Config;
     
       $user      = $config->data['db']['user'];
       $password  = $config->data['db']['password'];
       $dsn       = $config->data['db']['dsn'].';'.$config->data['db']['dbname'];
       $this->dbh = new PDO($dsn, $user, $password);

       return $this;
    }
    
    public function query(string $sql, array $params=[], string $class = stdClass::class): array
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
        
        return $sth->fetchAll(PDO::FETCH_CLASS, $class);
    }


    public function execute(string $sql, array $params=[]): bool
    {
        $sth = $this->dbh->prepare($sql);
        
        return $sth->execute($params);
    }

    public function lastInsertId(){
        return $this->dbh->lastInsertId();
    }

} //fin class Db


