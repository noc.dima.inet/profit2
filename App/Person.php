<?php

namespace App;

use App\Model;

class Person extends Model

{
    public string $name;
    public int $age;

    protected static string $table = 'persons';


}