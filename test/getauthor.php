<?php

require_once __DIR__ . '/../App/Config.php';
require_once __DIR__ . '/../App/Db.php';
require_once __DIR__ . '/../App/GetAndSetObjectTrait.php';
require_once __DIR__ . '/../App/HasAuthorInterface.php';
require_once __DIR__ . '/../app/View.php';
require_once __DIR__ . '/../App/Model.php';
require_once __DIR__ . '/../App/Article.php';
require_once __DIR__ . '/../App/Authors.php';
require_once __DIR__ . '/../App/News.php';

class News extends Model implements HasAuthorInterface
{
    public string $description;
    public string $text;
    public int|null $author_id;

    protected static string $table = 'news';

    $limitNews = News::findLimit(3);

    public function getAuthor() 
    {
        return $this->Authors::findById($this->author_id);
    }

}
?>

<h1> Бобро пожаловать! </h1>
<table>
<tr>
   <th>ID</th>
   <th>Название</th>
   <th>Автор</th>
   <th>Читать</th>
</tr>
<?php foreach($limitNews as $article): ?>
<?php /** @var \App\Article $article */ ?>
<tr>
   <td> &#9749; </td>
   <td><?php echo $article->description;?></td>
   <td><?php echo $article->$getAuthor();?></td>
   <td>  <a href="./article.php?id=<?php echo $article->id;?>" title="читка">
               Читать</a>
   </td>
</tr>

