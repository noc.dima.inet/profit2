<?php

require_once __DIR__ . '/../App/Config.php';
require_once __DIR__ . '/../App/Db.php';
require_once __DIR__ . '/../App/GetAndSetObjectTrait.php';
require_once __DIR__ . '/../App/Model.php';
require_once __DIR__ . '/../App/View.php';
require_once __DIR__ . '/../App/Person.php';
require_once __DIR__ . '/../App/Article.php';

$id= 4;

$article = new Article;
$article->id = 20;
$article->author = "Видосик М.";
$article->description = "Да, были люди в наше время";
$article->text = "Текст представляет собой отсылки к Лермонтову";
$article->save();
