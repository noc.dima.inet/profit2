<?php

use  App\{Article, View};

require __DIR__ . '/autoload.php';

$id = $_GET['id'];

if(!Article::findById($id)) {
    header('Location: ./404.php');
    exit;
}

$view = new View;

$view->article = Article::findById($id);
$view->display( __DIR__ . '/template/article.php');