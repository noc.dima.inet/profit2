<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="./style.css" rel="stylesheet" type="text/css" />

  <title> 404 страница </title>
</head>
<body>
<header>
  <nav class="navbar navbar-expand-md navbar-light bg-light">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="./index.php">Главная <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./news.php">Новости</a>
      </li>
    </ul>
  </div>
</nav>
</header> 
<!-- header -->
 
<main role="main" class="container">
 
<div class="container">
         
<p class="breadcrumb-item active" aria-current="page"> 
            <a href="./index.php"> Главная</a> / Такой страницы не существует </p>
 
         
        </nav>
    </div>
</div>
<div class="container">
    <div>
       
         <h1>Такой страницы не существует</h1>
         <p class="text-center">
             <img src="../i/404.jpg" alt="404 про ёжика" />
        </p>
 
    </div>
 
    
</div>
    <div class="row">
        <div class="col bg-light">
        <a href="./index.php"> Вернуться на главную</a>
        </div>
    </div>
</div>
 
</main>
<!-- Main -->

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
 
</footer>
<!-- Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>

 