<?php

use  App\{Article, Authors};

require __DIR__ . '/../autoload.php';

$article = new Article;
$author  = new Authors;

isset($_POST['id']) ? $article->id  = $_POST['id'] : $article->id  = (int)null;
!empty($_POST['author']) ? $author->author  = trim(htmlspecialchars($_POST['author'])) : $author->author  = null;

if (isset($_POST['del'])) {
    $article->delete();
    header("Location: /../admin.php");
    exit;
  }

  $article->description = trim(htmlspecialchars($_POST['description']));
  $article->text        = $_POST['text'];  
  
  if (isset($_POST['edit']) ) {   

    $article->author_id   = (int)$_POST['author_id']; 
    $author->id  = $article->author_id;

    if(Authors::findNameAuthor($author->author)) {
      $article->author_id =  Authors::findNameAuthor($author->author)->id;
      $article->update();
      $author->update();
    } elseif(null === $author->author){
      $article->author_id = null;
      $article->update();
    } else {
      $article->author_id = $author->insert();
      $article->update();
    }
  }

  if( isset($_POST['add']) ) {
 
    if(Authors::findNameAuthor($author->author)) {
        $article->author_id =  Authors::findNameAuthor($author->author)->id;
        $article->insert();
      }elseif(null === $author->author){
        $article->author_id = null;
        $article->insert();
      } else {
        $article->author_id = $author->insert();
        $article->insert();
      }
  }
  
  header("Location: /../admin.php");
  exit;



