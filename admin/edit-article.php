<?php

use  App\{Pages, Article, View};

require __DIR__ . '/../autoload.php';

$view = new View;

$view->title = Pages::findByName('edit-article')->title;
$view->article = Article::findById($_GET['id']);
$view->display( __DIR__ . '/template/edit-article.php');
 


