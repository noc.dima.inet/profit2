<?php

use  App\{Pages, View};

require __DIR__ . '/../autoload.php';

$view = new View;

$view->title = Pages::findByName('add-article')->title;
$view->display( __DIR__ . '/template/add-article.php');
 

