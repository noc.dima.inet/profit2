<?php

spl_autoload_register(function ($class) {
    $path = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
    // var_dump($path);die;
    if(file_exists($path)) {
        require $path;
    }
});
