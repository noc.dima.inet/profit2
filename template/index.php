<?php
//  var_dump($limitNews->author);die;
?>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="./style.css" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
  <title><?php echo $title; ?> </title>
</head>
<body>
<header>
  <nav class="navbar navbar-expand-md navbar-light bg-light">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="./index.php">Главная <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./news.php">Новости</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./admin.php">Войти</a>
      </li>
    </ul>
  </div>
</nav>
</header> 
<!-- header -->
 
<main role="main" class="container">
<div class="container">
    <div>
       
         <h1> Бобро пожаловать! </h1>
         <table>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Автор</th>
            <th>Читать</th>
        </tr>
        <?php foreach($limitNews as $article): ?>
          <?php /**  @var \App\Article $article */ ?>
        <tr>
            <td> &#9749; </td>
            <td><?php echo $article->description;?></td>
            <td><?php echo $article->author;?></td>
            <td>  <a href="./article.php?id=<?php echo $article->id;?>" title="читка">
                        Читать</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
 
    </div>
    
</div>
 
</main>
<!-- Main -->

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
 
</footer>
<!-- Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>