<?php

use  App\{Pages, Article, View};

require __DIR__ . '/autoload.php';

$view = new View;

$view->title = Pages::findByName('news')->title;

$view->allNews = Article::findAll();
$view->display('template/news.php');