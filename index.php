<?php

use  App\{Pages, Article, View};

require __DIR__ . '/autoload.php';

$view = new View;

$view->title = Pages::findByName('index')->title;
$view->limitNews = Article::findLimit(3);
$view->display(__DIR__ . '/template/index.php');

